import glob
import textract
import spacy
import pandas as pd

#loading model-best model
nlp_ner = spacy.load("/home/administrator/SkillCenter/Fields/Tools/model-best")

# Define the input folder path
input_folder_path = "/home/administrator/SkillCenter/Resumes/Test_Resumes"

# List of supported file extensions
supported_extensions = [".pdf", ".docx", ".doc", ".txt"]

# Get a list of all input files with supported extensions in the input foldesr
input_files = []
for ext in supported_extensions:
    input_files.extend(glob.glob(input_folder_path + "/*" + ext))

# Load the spaCy model for named entity recognition
nlp_ner = spacy.load("/home/administrator/SkillCenter/Fields/Tools/model-best")

# Initialize lists for storing entities and file names
tool_entities_list = []
hw_entities_list = []
domain_entities_list = []
file_name_list = []

def get_tools(ner_output):
    tool_entities = []
    tool_entities_list = []
    tools = []
    for ent in ner_output:
        if ent.ent_type_ == 'TOOL':
            # If the entity is a beginning entity, add it to the list of entities
            if ent.ent_iob_ == 'B':
                tool_entities.append(ent.text)
            # If the entity is an inside entity, add it to the most recent entity in the list
            elif ent.ent_iob_ == 'I':
                tool_entities[-1] += ' ' + ent.text

    #file_name_list.append(file_path.split('/')[-2])
    tool_entities_clean = [pl.replace('\n', '') for pl in tool_entities]
    tools = set(tool_entities_clean)

    if tools == set() :
        tools = ''

    return tools

def get_hardwares(ner_output):
    hw_entities = []
    hardwares = []
    for ent in ner_output:
        if ent.ent_type_ == 'HARDWARE':
            # If the entity is a beginning entity, add it to the list of entities
            if ent.ent_iob_ == 'B':
                hw_entities.append(ent.text)
            # If the entity is an inside entity, add it to the most recent entity in the list
            elif ent.ent_iob_ == 'I':
                hw_entities[-1] += ' ' + ent.text

    #file_name_list.append(file_path.split('/')[-2])
    hw_entities_clean = [pl.replace('\n', '') for pl in hw_entities]
    hardwares = set(hw_entities_clean)

    if hardwares == set() :
        hardwares = ''
    
    return hardwares

def get_domains(ner_output):
    domain_entities = []
    domains = []
    for ent in ner_output:
        if ent.ent_type_ == 'DOMAIN':
            # If the entity is a beginning entity, add it to the list of entities
            if ent.ent_iob_ == 'B':
                domain_entities.append(ent.text)
            # If the entity is an inside entity, add it to the most recent entity in the list
            elif ent.ent_iob_ == 'I':
                domain_entities[-1] += ' ' + ent.text

    #file_name_list.append(file_path.split('/')[-2])
    domain_entities_clean = [pl.replace('\n', '') for pl in domain_entities]
    domains = set(domain_entities_clean)
    
    if domains == set() :
        domains = ''
    
    return domains

# Loop through each input file and extract entities
for file_path in input_files:
    # Use textract to extract text from the input file
    text = textract.process(file_path).decode('utf-8')

    # Use the NER pipeline to extract entities
    ner_output = nlp_ner(text)

    # Extract the file name from the file path and add it to the file_name_list
    file_name_list.append(file_path.split('/')[-1])
    
    tools = get_tools(ner_output)
    hardwares = get_hardwares(ner_output)
    domains = get_domains(ner_output)

    tool_entities_list.append(tools)
    hw_entities_list.append(hardwares)
    domain_entities_list.append(domains)

df = pd.DataFrame({'File Name': file_name_list,
                   'Tools': tool_entities_list,
                   'Hardwares': hw_entities_list,
                   'Domains' : domain_entities_list})
#print(df)
# Write the dataframe to a CSV file
df.to_csv('/home/administrator/SkillCenter/Fields/Hardware/Result.csv', index=False)