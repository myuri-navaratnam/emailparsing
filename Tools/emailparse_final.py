import imaplib
import email
import time
import html2text
import os
import re
from email.utils import getaddresses
import glob
import textract
import spacy
import pandas as pd
from transformers import pipeline, AutoTokenizer, AutoModelForTokenClassification

# IMAP server settings
imap_server = 'imap.outlook.com'
username = 'myuri.n.v@outlook.com'
password = 'Tel@12345'
value = "value"

# Connect to the server
imap_conn = imaplib.IMAP4_SSL(imap_server)

# Login to the account
imap_conn.login(username, password)

# Select the inbox
imap_conn.select('INBOX')

# search for all emails
status, email_ids = imap_conn.search(None, "ALL")

#building a spacy model
def model_building():
    # Define the input folder path
    input_folder_path = "/home/nmyuri/SkillCenter/Downloads"

    # List of supported file extensions
    supported_extensions = [".docx", ".doc", ".txt", ".pdf"]

    # Get a list of all input files with supported extensions in the input folder
    input_files = []
    for ext in supported_extensions:
        input_files.extend(glob.glob(input_folder_path + "/*" + ext))

    # Define the paths to the custom NER models
    QSU_nlp = spacy.load("/home/nmyuri/SkillCenter/models/QSUmodel-best")
    THD_nlp = spacy.load("/home/nmyuri/SkillCenter/Fields/Skills/Tools/THD-model-best")
    ORP_nlp = spacy.load("/home/nmyuri/SkillCenter/Fields/Skills/OS/ORP-model-best")

    # Initialize pipeline for huggingface transformer to use pretrained model 
    tokenizer = AutoTokenizer.from_pretrained("Jean-Baptiste/roberta-large-ner-english")
    model = AutoModelForTokenClassification.from_pretrained("Jean-Baptiste/roberta-large-ner-english")
    name_nlp = pipeline('ner', model=model, tokenizer=tokenizer, aggregation_strategy="simple")


    # Initialize lists for storing entities and file names
    univ_entities_list =[]
    qua_entities_list = []
    spec_entities_list = []
    tool_entities_list = []
    hw_entities_list = []
    domain_entities_list = []
    pl_entities_list = []
    os_entities_list = []
    rtos_entities_list = []
    name_list = []
    file_name_list = []

    # Loop through each input file and extract entities
    for file_path in input_files:
        # Use textract to extract text from the input file
        text = textract.process(file_path).decode('utf-8')

        # Use the NER pipeline to extract entities
        QSU_ner_output = QSU_nlp(text)
        THD_ner_output = THD_nlp(text)
        ORP_ner_output = ORP_nlp(text)
        name_ner_output = name_nlp(text)

        # Initialize variables for storing entities
        univ_entities = []
        qua_entities = []
        spec_entities = []
        tool_entities = []
        hw_entities = []
        domain_entities = []
        pl_entities = []
        os_entities = []
        rtos_entities = []
        name = None

        # Get the name from the first "PER" entity, if any
        per_entity = next((ent['word'] for ent in name_ner_output if ent['entity_group'] == 'PER'), None)
        if per_entity:
            name = per_entity.strip()

        # Loop through each entity and group adjacent entities with the same label
        for ent in QSU_ner_output:
            if ent.ent_type_ == 'UNIV':
                # If the entity is a beginning entity, add it to the list of entities
                if ent.ent_iob_ == 'B':
                    univ_entities.append(ent.text)
                # If the entity is an inside entity, add it to the most recent entity in the list
                elif ent.ent_iob_ == 'I':
                    univ_entities[-1] += ' ' + ent.text
            if ent.ent_type_ == 'QUA':
                # If the entity is a beginning entity, add it to the list of entities
                if ent.ent_iob_ == 'B':
                    qua_entities.append(ent.text)
                # If the entity is an inside entity, add it to the most recent entity in the list
                elif ent.ent_iob_ == 'I':
                    qua_entities[-1] += ' ' + ent.text
            elif ent.ent_type_ == 'SPEC':
                # If the entity is a beginning entity, add it to the list of entities
                if ent.ent_iob_ == 'B':
                    spec_entities.append(ent.text)
                # If the entity is an inside entity, add it to the most recent entity in the list
                elif ent.ent_iob_ == 'I':
                    spec_entities[-1] += ' ' + ent.text
    
        # Loop through each entity and group adjacent entities with the same label
        for ent in THD_ner_output:
            if ent.ent_type_ == 'TOOL':
                # If the entity is a beginning entity, add it to the list of entities
                if ent.ent_iob_ == 'B':
                    tool_entities.append(ent.text)
                # If the entity is an inside entity, add it to the most recent entity in the list
                elif ent.ent_iob_ == 'I':
                    tool_entities[-1] += ' ' + ent.text
            if ent.ent_type_ == 'HARDWARE':
                # If the entity is a beginning entity, add it to the list of entities
                if ent.ent_iob_ == 'B':
                    hw_entities.append(ent.text)
                # If the entity is an inside entity, add it to the most recent entity in the list
                elif ent.ent_iob_ == 'I':
                    hw_entities[-1] += ' ' + ent.text
            elif ent.ent_type_ == 'DOMAIN':
                # If the entity is a beginning entity, add it to the list of entities
                if ent.ent_iob_ == 'B':
                    domain_entities.append(ent.text)
                # If the entity is an inside entity, add it to the most recent entity in the list
                elif ent.ent_iob_ == 'I':
                    domain_entities[-1] += ' ' + ent.text
    
        # Loop through each entity and group adjacent entities with the same label
        for ent in ORP_ner_output:
            if ent.ent_type_ == 'OS':
                # If the entity is a beginning entity, add it to the list of entities
                if ent.ent_iob_ == 'B':
                    os_entities.append(ent.text)
                # If the entity is an inside entity, add it to the most recent entity in the list
                elif ent.ent_iob_ == 'I':
                    os_entities[-1] += ' ' + ent.text
            if ent.ent_type_ == 'RTOS':
                # If the entity is a beginning entity, add it to the list of entities
                if ent.ent_iob_ == 'B':
                    rtos_entities.append(ent.text)
                # If the entity is an inside entity, add it to the most recent entity in the list
                elif ent.ent_iob_ == 'I':
                    rtos_entities[-1] += ' ' + ent.text
            elif ent.ent_type_ == 'PROGRAMMING_LANGUAGE':
                # If the entity is a beginning entity, add it to the list of entities
                if ent.ent_iob_ == 'B':
                    pl_entities.append(ent.text)
                # If the entity is an inside entity, add it to the most recent entity in the list
                elif ent.ent_iob_ == 'I':
                    pl_entities[-1] += ' ' + ent.text


        # Extract the file name from the file path and add it to the file_name_list
        file_name_list.append(file_path.split('/')[-1])
        univ_entities_clean = [univ.replace('\n', '') for univ in univ_entities]
        qua_entities_clean = [qual.replace('\n', '') for qual in qua_entities]
        spec_entities_clean = [spec.replace('\n', '') for spec in spec_entities]
        tool_entities_clean = [tool.replace('\n', '') for tool in tool_entities]
        hw_entities_clean = [hw.replace('\n', '') for hw in hw_entities]
        domain_entities_clean = [domain.replace('\n', '') for domain in domain_entities]
        os_entities_clean = [os.replace('\n', '') for os in os_entities]
        rtos_entities_clean = [rtos.replace('\n', '') for rtos in rtos_entities]
        pl_entities_clean = [pl.replace('\n', '') for pl in pl_entities]

        # Get the unique values of the extracted entities
        univ_unique = list(set(univ_entities))
        qua_unique = list(set(qua_entities))
        spec_unique = list(set(spec_entities))
        tool_unique = list(set(tool_entities))
        hw_unique = list(set(hw_entities))
        domain_unique = list(set(domain_entities))
        pl_unique = list(set(pl_entities))
        os_unique = list(set(os_entities))
        rtos_unique = list(set(rtos_entities))

        # Join the entities with a comma and space
        univ_entities_str = ', '.join(univ_unique)
        qua_entities_str = ', '.join(qua_unique)
        spec_entities_str = ', '.join(spec_unique)
        tool_entities_str = ', '.join(tool_unique)
        hw_entities_str = ', '.join(hw_unique)
        domain_entities_str = ', '.join(domain_unique)
        pl_entities_str = ', '.join(pl_unique)
        os_entities_str = ', '.join(os_unique)
        rtos_entities_str = ', '.join(rtos_unique)


        # Add the entities to the corresponding lists
        univ_entities_list.append(univ_entities_str)
        qua_entities_list.append(qua_entities_str)
        spec_entities_list.append(spec_entities_str)
        tool_entities_list.append(tool_entities_str)
        hw_entities_list.append(hw_entities_str)
        domain_entities_list.append(domain_entities_str)
        pl_entities_list.append(pl_entities_str)
        os_entities_list.append(os_entities_str)
        rtos_entities_list.append(rtos_entities_str)
        name_list.append(name)
        #file_name_list.append(file_path.name)
    
        # Add the entities to the corresponding lists
        univ_entities_list.append(univ_entities_clean)
        qua_entities_list.append(qua_entities_clean)
        spec_entities_list.append(spec_entities_clean)
        tool_entities_list.append(tool_entities_str)
        hw_entities_list.append(hw_entities_str)
        domain_entities_list.append(domain_entities_str)
        pl_entities_list.append(pl_entities_str)
        os_entities_list.append(os_entities_str)
        rtos_entities_list.append(rtos_entities_str)
        name_list.append(name)
        #file_name_list.append(file_path.name)

    # Create a dataframe with the entities and file names
    df = pd.DataFrame({
            'Name': name_list,
            'University': univ_entities_list,
            'Qualification': qua_entities_list,
            'Specialization': spec_entities_list,
            'Tools': tool_entities_list,
            'Hardware': hw_entities_list,
            'Domain': domain_entities_list,
            'Programming Languages': pl_entities_list,
            'OS': os_entities_list,
            'RTOS': rtos_entities_list})
    print(df)
    # Write the dataframe to a CSV file
    df.to_csv('/home/nmyuri/SkillCenter/Results_final.csv', index=False)

def get_mailbody(imap_conn,email_id):
    # Fetch the email message
    status, email_data = imap_conn.fetch(email_id, '(RFC822)')
    raw_email = email_data[0][1].decode('utf-8')

    # Parse the email message
    email_message = email.message_from_string(raw_email)

    # Extract the subject and body of the email
    subject = email_message['Subject']  
    #body = email_message.get_body().get_payload(decode=True).decode('utf-8')
    payload = email_message.get_payload()
    if isinstance(payload, list):
        body = ''.join(str(p.get_payload(decode=True), 'utf-8') for p in payload if p.get_content_type() == 'text/plain')
    elif payload.get_content_type() == 'text/plain':
        body = str(payload.get_payload(decode=True), 'utf-8')
    else:
        body = None
    # Do something with the subject and body
    print('Subject:', subject)
    print('Body:', body)


def download_attachment(imap_conn,emailid):
  # fetch the email message and mark it as seen
    resp, data = imap_conn.fetch(emailid, '(RFC822)')
    imap_conn.store(emailid, '+FLAGS', '\\Seen')

    # parse the email message
    email_message = email.message_from_bytes(data[0][1])
    #default_dir = os.getcwd()

    # loop through the parts of the email message
    for part in email_message.walk():
        # if the part is an attachment
        if part.get_content_disposition() and 'attachment' in part.get_content_disposition().lower():
            # get the filename and attachment data
            filename = part.get_filename()
            attachment = part.get_payload(decode=True)

            # save the attachment to disk
            if filename:
                #with open(os.path.join(default_dir,filename),'wb') :
                with open(os.path.join('/home/nmyuri/SkillCenter/Downloads', filename), 'wb') as f:
                    f.write(attachment)
                print("Attachment is downloaded!")
        

def get_sendermailid(imap_conn,email_id):
    # fetch the email message header
    status, email_data = imap_conn.fetch(email_id, "(RFC822.HEADER)")
    
    # convert the email message header to a string
    email_header = email_data[0][1].decode("utf-8")
    
    # create an email.message.Message object from the header string
    email_message = email.message_from_string(email_header)
    
    # extract the email id and subject from the header
    sender_email = email_message["From"]
    match = re.search(r'<.*>', sender_email)
    subject = email_message["Subject"]

    if match:
        email_id = match.group(0)[1:-1]  # remove the "<" and ">" characters
    else:
        email_id = sender_email  # if no "<>" characters found, use the entire sender email as the ID
    
    # print the email id and subject
    print("Email-ID: {}".format(email_id))
    #print("Subject: {}".format(subject))

# wait for new messages to arrive
while True:
    # search for new messages
    status, msg_ids = imap_conn.search(None, 'UNSEEN')
    if msg_ids != [b'']:
        for msg_id in msg_ids[0].split():
            # fetch the email and parse it
            status, email_data = imap_conn.fetch(msg_id, '(RFC822)')
            email_message = email.message_from_bytes(email_data[0][1])
            email_subject = email_message['Subject']
            email_from = email.utils.parseaddr(email_message['From'])[1]
            email_body = ""

            # extract the email body
            if email_message.is_multipart():
                for part in email_message.walk():
                    if part.get_content_type() == "text/plain":
                        email_body += part.get_payload(decode=True).decode('utf-8')
                    elif part.get_content_type() == "text/html":
                        email_body = html2text.html2text(part.get_payload(decode=True).decode('utf-8'))
            else:
                email_body = email_message.get_payload(decode=True).decode('utf-8')

            # print the email details
            print(f"New email received from {email_from} : {email_subject}")
            print(f"Email body : \n{email_body}")
            get_sendermailid(imap_conn,msg_id)
            download_attachment(imap_conn,msg_id)
            
    # wait for some time before checking for new messages again
    time.sleep(10)

# close the connection
imap_conn.close()
imap_conn.logout()
