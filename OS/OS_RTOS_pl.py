import glob
import textract
import spacy
import pandas as pd

#loading model-best model
nlp_ner = spacy.load("/home/administrator/SkillCenter/Fields/Tools/model-best_pl")

# Define the input folder path
input_folder_path = "/home/administrator/SkillCenter/Resumes/Test_Resumes"

# List of supported file extensions
supported_extensions = [".pdf", ".docx", ".doc", ".txt"]

# Get a list of all input files with supported extensions in the input foldesr
input_files = [s]
for ext in supported_extensions:
    input_files.extend(glob.glob(input_folder_path + "/*" + ext))

# Load the spaCy model for named entity recognition
nlp_ner = spacy.load("/home/administrator/SkillCenter/Fields/Tools/model-best_pl")

# Initialize lists for storing entities and file names
pl_entities_list = []
os_entities_list = []
rtos_entities_list = []
file_name_list = []

def get_pl(ner_output):
    pl_entities = []
    pl = []
    for ent in ner_output:
        if ent.ent_type_ == 'PROGRAMMING_LANGUAGE':
            # If the entity is a beginning entity, add it to the list of entities
            if ent.ent_iob_ == 'B':
                pl_entities.append(ent.text)
            # If the entity is an inside entity, add it to the most recent entity in the list
            elif ent.ent_iob_ == 'I':
                pl_entities[-1] += ' ' + ent.text

    #file_name_list.append(file_path.split('/')[-2])
    pl_entities_clean = [i.replace('\n', '') for i in pl_entities]
    pl = set(pl_entities_clean)

    if pl == set() :
        pl = ''

    return pl

def get_os(ner_output):
    os_entities = []
    os = []
    for ent in ner_output:
        if ent.ent_type_ == 'OS':
            # If the entity is a beginning entity, add it to the list of entities
            if ent.ent_iob_ == 'B':
                os_entities.append(ent.text)
            # If the entity is an inside entity, add it to the most recent entity in the list
            elif ent.ent_iob_ == 'I':
                os_entities[-1] += ' ' + ent.text

    #file_name_list.append(file_path.split('/')[-2])
    os_entities_clean = [pl.replace('\n', '') for pl in os_entities]
    os = set(os_entities_clean)

    if os == set() :
        os = ''
    
    return os

def get_rtos(ner_output):
    rtos_entities = []
    rtos = []
    for ent in ner_output:
        if ent.ent_type_ == 'RTOS':
            # If the entity is a beginning entity, add it to the list of entities
            if ent.ent_iob_ == 'B':
                rtos_entities.append(ent.text)
            # If the entity is an inside entity, add it to the most recent entity in the list
            elif ent.ent_iob_ == 'I':
                rtos_entities[-1] += ' ' + ent.text

    #file_name_list.append(file_path.split('/')[-2])
    rtos_entities_clean = [pl.replace('\n', '') for pl in rtos_entities]
    rtos = set(rtos_entities_clean)
    
    if rtos == set() :
        rtos = ''
    
    return rtos

# Loop through each input file and extract entities
for file_path in input_files:
    # Use textract to extract text from the input file
    text = textract.process(file_path).decode('utf-8')

    # Use the NER pipeline to extract entities
    ner_output = nlp_ner(text)

    # Extract the file name from the file path and add it to the file_name_list
    file_name_list.append(file_path.split('/')[-1])
    
    pl = get_pl(ner_output)
    os = get_os(ner_output)
    rtos = get_rtos(ner_output)

    pl_entities_list.append(pl)
    os_entities_list.append(os)
    rtos_entities_list.append(rtos)

df = pd.DataFrame({'File Name': file_name_list,
                   'Programming Languages': pl_entities_list,
                   'OS': os_entities_list,
                   'RTOS' : rtos_entities_list})
#print(df)
# Write the dataframe to a CSV file
df.to_csv('/home/administrator/SkillCenter/Fields/Hardware/Result.csv', index=False)
